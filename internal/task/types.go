package task

import (
	"io"
)

// Readliner is the interface for running jobs.
//
// Readline adds a string to a job as the starting input to process.
type Readliner interface {
	Readline(string) Jobber
}

// Jobber is the interface to a job.
//
// Do is used to do the processing for the job.
// Output writes the result to the io.Writer provided.
type Jobber interface {
	Do()
	Output(io.Writer)
}
