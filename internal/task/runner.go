package task

import (
	"bufio"
	"context"
	"io"
	"log"
	"os"
	"sync"
)

// Run controls the job pipeline.
func Run(ctx context.Context, rl Readliner, to io.Writer, num int) {
	var wg sync.WaitGroup

	in := make(chan Jobber)

	wg.Add(1)
	go func() {
		s := bufio.NewScanner(os.Stdin)
		for s.Scan() {
			select {
			case <-ctx.Done():
				break
			default:
				in <- rl.Readline(s.Text())
			}
		}

		if s.Err() != nil {
			log.Printf("stdin: %s", s.Err())
		}
		close(in)
		wg.Done()
	}()

	out := make(chan Jobber)

	for i := 0; i < num; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			for job := range in {
				// check for cancel, as the channel could still have many items
				select {
				case <-ctx.Done():
					return
				default:
					job.Do()
					out <- job
				}
			}
		}()
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	for job := range out {
		job.Output(to)
	}
}
