#! /bin/bash

if ! [ -d "$1" ]; then
  echo "Usage:
  install.sh <dir>"
  exit 1
fi

dir=${1%*/}
path="./$dir/..."
go install -ldflags "-w -s" -trimpath $path

echo "Installed $dir to $(go env GOBIN)"
