# Task Factory

This is a framework for creating task-specific command line apps.

An app will read standard input and then process each line using a
fan-out pattern using a worker pool. Results can be delivered to
standard output (the default) or to a specified file (`-o <filename>`).
The number of Goroutines (the default is 10) launched in the worker pool
is also configurable (`-n <int>`).

The app will perform a graceful shutdown upon recieving either a SIGINT
or SIGKILL signal. When a shutdown signal is recieveed:

1. Input reading will stop.
2. The worker pool will wind down, skipping any task that might be
   waiting.
3. Tasks processed prior to the shutdown signal will output their results.

## Usage

The main package directory should be copied so that seperate apps can be
built for each task.

- Copy the `./sample-task` directory to `./<my-task>`
- Edit `./<my-task>/job.go` to perform your task

To track changes from `sample-task/main.go` into sibling tasks, the file
can symlinked into the sibling directory.  Git will preserve the
relationship between the files, but the sibling project will break if
the link breaks (deleting the `sample-task` directory, for example).

To symlink `main.go`, run `ln -rsvf ./sample-task/main.go ./<my-task>/`

## Building / installing

- Build with `go build -o <name> ./<my-task>/...` in this case the
    `-o <name>` must be given, as the name taken from the directory
    will conflict with the default output name.
- With the provided build script: `./install.sh <my-task>`, this will
    print the installed location after a sucessful installation.
- Running `go install ./...` from the main repository directory will
    build and install all apps.

## Notes

The default behavior of `go install` is:

> Executables are installed in the directory named by the GOBIN
> environment variable, which defaults to $GOPATH/bin or $HOME/go/bin if
> the GOPATH environment variable is not set.

To have the app installed in a common place like `~/bin` use the `go env
-w` command.

```
go env -w GOBIN=$HOME/bin
```
