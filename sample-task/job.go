package main

import (
	"fmt"
	"io"
	"log"

	"go.local/task-factory/internal/task"
)

// Factory makes jobs.
type Factory struct{}

// Readline loads the job's input with str.
func (f Factory) Readline(str string) task.Jobber {
	r := new(job)
	r.input = str
	return r
}

type job struct {
	input  string
	result string
	err    error
}

// Do performs the work of processing the job.
func (j *job) Do() {
	j.result = jobProcessor(j.input)
}

// Output controls how the job's result is displayed.
func (j *job) Output(to io.Writer) {
	if j.err != nil {
		log.Printf("job: %s", j.err)
		return
	}

	// format output
	fmt.Fprintln(to, j.result)
}

// jobProcessor handles the job specific tasks
func jobProcessor(in string) string {
	return in
}
