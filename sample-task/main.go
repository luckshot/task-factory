package main

import (
	"context"
	"flag"
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"

	"go.local/task-factory/internal/task"
)

func main() {
	var (
		flagLoops  = flag.Int("n", 10, "Number of goroutines to start")
		flagOutput = flag.String("o", "stdout", "Output location")
	)
	flag.Parse()

	var out io.WriteCloser
	switch *flagOutput {
	case "stdout":
		out = os.Stdout
	default:
		fh, err := os.Create(*flagOutput)
		if err != nil {
			log.Printf("output: %s", err)
			return
		}
		out = fh
	}
	defer out.Close()

	quit := make(chan os.Signal, 1)
	ctx, cancel := context.WithCancel(context.Background())
	// Catch SIGINT and SIGKILL signals
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-quit
		log.Println("shutting down...")
		cancel()
	}()

	task.Run(ctx, new(Factory), out, *flagLoops)
}
